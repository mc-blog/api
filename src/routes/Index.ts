import APIError from './APIError';
import CommentsRoutes from './comments/CommentsRoutes';
import Endpoint from './Endpoint';
import PostRoutes from './posts/PostsRoutes';
import UsersRoutes from './users/UsersRoutes';

export default class Routes {
    private endpoints: Endpoint[];

    constructor(private server: any) {
        this.endpoints = [
            new PostRoutes(this.server),
            new CommentsRoutes(this.server),
            new UsersRoutes(this.server),
        ];
    }

    public prepare() {
        this.server.use((req, res, next) => {
            res.header('Access-Control-Allow-Origin', '*');
            res.header(
                'Access-Control-Allow-Headers',
                'Origin, X-Requested-With, Content-Type, Accept, Authorization',
            );
            if (req.method === 'OPTIONS') {
                res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
                return res
                    .status(200)
                    .json({});
            }
            next();
        });

        this.endpoints.forEach((endpoint) => endpoint.prepare());

        this.server.use((req, res, next) => {
            const error: APIError = {
                message: 'Not Found',
                status: 404,
            };
            next(error);
        });

        this.server.use((error: APIError, req, res, next) => {
            res
                .status(error.status || 500)
                .json({
                    error: {
                        message: error.message,
                    },
                });
        });
    }
}
