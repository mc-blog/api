export default class UsersRoutes {
    constructor(private server: any) {}

    public prepare() {
        this.server.get('/users', (req, res, next) => {
            res
                .status(200)
                .json({
                    message: 'GET /users - get all users',
                });
        });

        this.server.post('/users', (req, res, next) => {
            const { name } = req.body;
            res
                .status(201)
                .json({
                    message: 'POST /users - add new user',
                    user: {
                        name,
                    },
                });
        });

        this.server.get('/users/:userId', (req, res, next) => {
            const id = req.params.userId;
            res
                .status(200)
                .json({
                    message: `GET /users/${id} - get user`,
                });
        });

        this.server.patch('/users/:userId', (req, res, next) => {
            const id = req.params.userId;
            res
                .status(200)
                .json({
                    message: `PATCH /users/${id} - update user`,
                });
        });

        this.server.delete('/users/:userId', (req, res, next) => {
            const id = req.params.userId;
            res
                .status(200)
                .json({
                    message: `DELETE /users/${id} - delete user`,
                });
        });
    }
}
