import Comment from '../../models/Comment';
import Post from '../../models/Post';

export default class PostsRoutes {
    constructor(private server: any) {}

    public prepare() {
        this.server.get('/posts', async (req, res, next) => {
            let posts = null;
            try {
                posts = await Post.find().exec();
            } catch (error) {
                return res.status(500).json({ error });
            }

            const response = {
                count: posts.length,
                posts,
            };
            res.status(200).json(response);
        });

        this.server.post('/posts', async (req, res, next) => {
            const { author, content, title } = req.body;

            const postToSave = new Post({ author, content, title });

            let post = null;
            try {
                post = await postToSave.save();
            } catch (error) {
                return res.status(500).json({ error });
            }

            const response = {
                message: 'Created new post',
                post,
            };
            res.status(201).json(response);
        });

        this.server.get('/posts/:postId', async (req, res, next) => {
            const { postId } = req.params;

            let post = null;
            try {
                post = await Post.findById(postId).exec();
            } catch (error) {
                return res.status(500).json({ error });
            }

            res.status(200).json({ post });
        });

        this.server.patch('/posts/:postId', async (req, res, next) => {
            const { postId } = req.params;

            const postToUpdate = {};
            for (const prop of req.body) {
                postToUpdate[prop.name] = prop.value;
            }

            let post = null;
            try {
                post = await Post.findByIdAndUpdate(postId, postToUpdate, { new: true }).exec();
            } catch (error) {
                return res.status(500).json({ error });
            }

            const response = {
                message: 'Updated post',
                post,
            };
            res.status(200).json(response);
        });

        this.server.delete('/posts/:postId', async (req, res, next) => {
            const { postId } = req.params;

            try {
                await Post.findByIdAndRemove(postId).exec();
            } catch (error) {
                return res.status(500).json({ error });
            }

            res.status(200).json({ message: `Removed post (_id: ${postId})` });
        });

        this.server.get('/posts/:postId/comments', async (req, res, next) => {
            const { postId } = req.params;

            let comments = null;
            try {
                comments = await Comment.find().where({ postId }).exec();
            } catch (error) {
                return res.status(500).json({ error });
            }

            const response = {
                comments,
                count: comments.length,
            };

            res.status(200).json(response);
        });

        this.server.post('/posts/:postId/comments', async (req, res, next) => {
            const { postId } = req.params;
            const { author, content } = req.body;

            const commentToSave = new Comment({ author, content, postId });

            let comment = null;
            try {
                comment = await commentToSave.save();
            } catch (error) {
                return res.status(500).json({ error });
            }

            const response = {
                comment,
                message: 'Created new comment',
            };

            res.status(201).json(response);
        });
    }
}
