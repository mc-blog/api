export default interface APIError {
    message: string;
    status: number;
}
