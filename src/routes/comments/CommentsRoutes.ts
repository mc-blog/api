import Comment from '../../models/Comment';

export default class CommentsRoutes {
    constructor(private server: any) {}

    public prepare() {
        this.server.get('/comments/:commentId', async (req, res, next) => {
            const { commentId } = req.params;

            let comment = null;
            try {
                comment = await Comment.findById(commentId).exec();
            } catch (error) {
                return res.status(500).json({ error });
            }

            res.status(200).json({ comment });
        });

        this.server.patch('/comments/:commentId', async (req, res, next) => {
            const { commentId } = req.params;

            const commentToUpdate = {};
            for (const prop of req.body) {
                commentToUpdate[prop.name] = prop.value;
            }

            let comment = null;
            try {
                comment = await Comment.findByIdAndUpdate(commentId, commentToUpdate);
            } catch (error) {
                res.status(500).json({ error });
            }

            const response = {
                comment,
                message: 'Updated comment',
            };

            res.status(200).json(response);
        });

        this.server.delete('/comments/:commentId', async (req, res, next) => {
            const { commentId } = req.params;

            try {
                await Comment.findByIdAndRemove(commentId).exec();
            } catch (error) {
                return res.status(500).json({ error });
            }

            res.status(200).json({ message: `Removed comment (_id: ${commentId})` });
        });
    }
}
