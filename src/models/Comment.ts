import { model, Schema } from 'mongoose';

const commentSchema: Schema = new Schema({
    author: {
        id: {
            required: true,
            type: 'number',
        },
        name: {
            required: true,
            type: 'string',
        },
    },
    content: {
        required: true,
        type: 'string',
    },
    postId: {
        required: true,
        type: 'number',
    },
}, {
    timestamps: true,
    versionKey: false,
});

export default model('Comment', commentSchema);
