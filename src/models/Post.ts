import { model, Schema } from 'mongoose';

const postSchema: Schema = new Schema({
    author: {
        id: {
            required: true,
            type: 'number',
        },
        nick: {
            required: true,
            type: 'string',
        },
    },
    content: {
        required: true,
        type: 'string',
    },
    title: {
        required: true,
        type: 'string',
    },
}, {
    timestamps: true,
    versionKey: false,
});

export default model('Post', postSchema);
